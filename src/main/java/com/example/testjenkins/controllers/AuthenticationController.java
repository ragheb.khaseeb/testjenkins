package com.example.testjenkins.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController extends BaseController{
	
	@GetMapping(value = "/test")
	public String test() {
		return "ok";
	}
}
